import subprocess

class androidinput:
    def __init__(self):
        pass

    def tap(self, point):
        self.exec(["tap", str(point[0]), str(point[1])])

    def exec(self, args):
        try:
            cmd = ["adb", "shell", "input"] + args
            print(cmd)
            proc = subprocess.run(cmd)
            print(proc.stdout)
        except Exception as e:
            print(e)