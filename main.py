from PIL import Image
import io
import time
import numpy as np
import time
import lmdb
from skimage.measure import compare_ssim
import imutils
import cv2
import copy
import time
from inputcontrol import androidinput
from minicapconnecter import Connecter


env = lmdb.open('mylmdb', map_size=1024*1024*100*1024)

i = np.zeros((2160, 1080), np.uint8)
img_blank = cv2.imdecode(i, cv2.IMREAD_COLOR)

lastimage = None

cv2.namedWindow("display", cv2.WINDOW_KEEPRATIO)
cv2.resizeWindow("display", (540, 1080))

con = Connecter()
con.connect()
con.getHeader()

def click(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        print("{} {}".format(x, y))
        ai = androidinput()
        ai.tap((x, y))

cv2.setMouseCallback("display", click)


def imageupdate(num):
    start = time.clock()
    global lastimage

    try:
        frame = con.getNextFrame()

        nparr = np.frombuffer(frame, np.uint8)
        img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

        if lastimage is None:
            lastimage = img_np
            cv2.imshow("display", img_np)
        else:
            grayA = cv2.cvtColor(lastimage, cv2.COLOR_BGR2GRAY)
            grayB = cv2.cvtColor(img_np, cv2.COLOR_BGR2GRAY)

            (score, diff) = compare_ssim(grayA, grayB, full=True)
            diff = (diff * 255).astype("uint8")
            print("SSIM:{}".format(score))

            thresh = cv2.threshold(
                diff, 100, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
            cnts = cv2.findContours(
                thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            cnts = cnts[0] if imutils.is_cv2() else cnts[1]

            imageB = copy.deepcopy(img_np)
            lastimage = img_np

            for c in cnts:
                (x, y, w, h) = cv2.boundingRect(c)
                if w * h < 1000:
                    continue
                cv2.rectangle(imageB, (x, y), (x+w, y+h), (0, 0, 255), 2)

            cv2.imshow("display", imageB)

        with env.begin(write=True) as txn:
            txn.put(str(num).encode('ascii'), frame)

    except Exception as e:
        print(e)

    end = time.clock()
    print(end - start)

count = 0
while True:
    imageupdate(count)
    cv2.waitKey(1)
    count += 1

cv2.waitKey(0)