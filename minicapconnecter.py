import socket
import struct
import time

class Connecter:
    def __init__(self):
        self.address = ('127.0.0.1', 1313)
 
    def connect(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect(self.address)

    def getHeader(self):
        header = self.s.recv(24)

        print(header[0])
        print(header[1])
        print(self.toint(header[2:6]))
        print(self.toint(header[6:10]))
        print(self.toint(header[10:14]))
        print(self.toint(header[14:18]))
        print(self.toint(header[18:22]))
        print(header[22])
        print(header[23])

        # sleep for frame ready
        time.sleep(1)

    def getNextFrame(self):
        n = self.s.recv(4)
        nf = self.toint(n)

        frame = self.s.recv(nf)
        return frame

    def toint(self, bytearray):
        lo, hi = struct.unpack("<HH", bytearray)
        return (hi << 16) | lo